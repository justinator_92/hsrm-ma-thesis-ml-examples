#!/usr/bin/env python3

import os
import struct
import numpy as np
import matplotlib.pyplot as plt

import cv2
from neuralnetwork import *



def load_mnist(path, kind='train'):
    """Load MNIST data from 'path'"""
    labels_path = os.path.join(path, '%s-labels-idx1-ubyte' % kind)
    images_path = os.path.join(path, '%s-images-idx3-ubyte' % kind)

    with open(labels_path, 'rb') as lbpath:
        magic, n = struct.unpack('>II', lbpath.read(8))
        labels = np.fromfile(lbpath, dtype=np.uint8)

    with open(images_path, 'rb') as imgpath:
        magic, num, rows, cols = struct.unpack(">IIII", imgpath.read(16))
        images = np.fromfile(imgpath, dtype=np.uint8).reshape(len(labels), 784)

    return images, labels


def test_images():
    """Read and write the first 10 images of the MNIST data set to file system"""
    images, labels = load_mnist('mnist')
    for i in range(0, 10):
        img = images[i]
        cv2.imwrite("test_" + str(i) + ".png", img.reshape(28, 28))

def show_images():
    images, labels = load_mnist('mnist', kind='train')
    fig, ax = plt.subplots(nrows=2, ncols=5, sharex=True, sharey=True)
    ax = ax.flatten()
    for i in range(10):
        img = images[y_train == i][0].reshape(28, 28)
        ax[i].imshow(img, cmap='Greys', interpolation='nearest')
    ax[0].set_xticks([])
    ax[0].set_yticks([])
    plt.tight_layout()
    plt.show()

def show_images(number):
    images, labels = load_mnist('mnist', kind='train')
    fix, ax = plt.subplots(nrows=5, ncols=5, sharex=True, sharey=True)
    ax = ax.flatten()
    for i in range(25):
        img = images[labels == number][i].reshape(28, 28)
        ax[i].imshow(img, cmap='Greys', interpolation='nearest')
    ax[0].set_xticks([])
    ax[0].set_yticks([])
    plt.tight_layout()
    plt.show()

def transform_to_CSV():
    train_images, train_labels = load_mnist('mnist', kind='train')
    eval_images, eval_labels = load_mnist('mnist', kind='t10k')

    np.savetxt('train_img.csv', train_images, fmt='%i', delimiter=',')
    np.savetxt('train_labels.csv', train_labels, fmt='%i', delimiter=',')
    np.savetxt('test_img.csv', eval_images, fmt='%i', delimiter=',')
    np.savetxt('test_labels.csv', eval_labels, fmt='%i', delimiter=',')

def load_from_CSV():
    X_train = np.genfromtxt('train_img.csv', dtype=int, delimiter=',')
    y_train = np.genfromtxt('train_labels.csv', dtype=int, delimiter=',')
    X_test = np.genfromtxt('test_img.csv', dtype=int, delimiter=',')
    y_test = np.genfromtxt('test_img.csv', dtype=int, delimiter=',')
    return X_train, y_train


X_train, y_train = load_mnist('mnist', kind='train')
print('Rows: %d, columns: %d' % (X_train.shape[0], X_train.shape[1]))

X_test, y_test = load_mnist('mnist', kind='t10k')
print('Rows: %d, columns: %d' % (X_test.shape[0], X_test.shape[1]))

#show_images(8)
#test_images()
#transform_to_CSV()

nn = NeuralNetMLP(n_output=10,
                  n_features=X_train.shape[1],
                  n_hidden=50,
                  l2=0.1,
                  l1=0.0,
                  epochs=1000,
                  eta=0.001,
                  alpha=0.001,
                  decrease_const=0.00001,
                  shuffle=True,
                  minibatches=50,
                  random_state=1)

nn.fit(X_train, y_train, print_progress=True)

plt.plot(range(len(nn.cost_)), nn.cost_)
plt.ylim([0, 2000])
plt.ylabel('Cost')
plt.xlabel('Epochs * 50')
plt.tight_layout()
plt.show()
