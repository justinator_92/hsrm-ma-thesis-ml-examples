import tensorflow as tf

sess = tf.Session()

# create a dense layer taht takes a batch of input vectors
# and produces a single output value for each

x = tf.placeholder(tf.float32, shape=[None, 3])
linear_model = tf.layers.Dense(units=1)  # perfroms weighted sum over vector elements
y = linear_model(x)

init = tf.global_variables_initializer()  # only initializes data which was alredy present in the graph when initializer was created
sess.run(init)

print(sess.run(y, {x: [[1,2,3],[4,5,6]]}))

# TF offers shortcut functions, the following code is doing the same
x = tf.placeholder(tf.float32, shape=[None, 3])
y = tf.layers.dense(x, units=1)

init = tf.global_variables_initializer()
sess.run(init)

print(sess.run(y, {x: [[1,2,3],[4,5,6]]}))


